# Presentaciones: JQUERY, javascript aplicado a la web

Proyecto generado con [GitLab Pages](https://pages.gitlab.io) para su uso como presentaciones del curso: JQUERY, javascript aplicado a la web.

Las páginas generadas se pueden acceder desde: [Índice](https://formacionjquery.gitlab.io/presentaciones)

Se han generado pensando en ser enlazadas desde la wiki del proyecto: [Wiki](https://gitlab.com/FormacionJQuery/jquery-javascript-aplicado-a-web/-/wikis/home)
